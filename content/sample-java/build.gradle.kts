import com.soprasteria.dep.*
import org.springframework.cloud.contract.verifier.plugin.ContractVerifierExtension

depJavaApplication {
    apiVersion = 1
    useJib = true
    usePropertiesCompletion = false
    useKapt = false
    useDok = false
    useDokDsl = false
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation(dep("api"))
    implementation(dep("mongodb"))
    implementation(dep("security"))
    runtimeOnly(dep("monitor"))
    testImplementation(dep("test"))
}

configure<ContractVerifierExtension> {
    // Override configuration class for testing contracts in order to customize the business context
    setBaseClassForTests("com.soprasteria.dep.quiz.QuizContractVerifierBase")
}

val apiElements = project.configurations.named("apiElements")
val runtimeElements = project.configurations.named("runtimeElements")
val jar: Task = project.tasks.getByName("jar")
val bootJar: Task = project.tasks.getByName("bootJar")
setOf(apiElements, runtimeElements).forEach {
    it.get().outgoing.artifacts.removeIf { a -> a.buildDependencies.getDependencies(null).contains(jar) }
    it.get().outgoing.artifact(bootJar)
}
