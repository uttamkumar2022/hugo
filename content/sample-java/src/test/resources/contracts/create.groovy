import org.springframework.cloud.contract.spec.Contract

Contract.make {
    def anyId = '[a-f0-9]{24}'
    def anyNewDuration = "PT([0-5]?[0-9]S)"
    def anyDuration = "PT([0-5]?[0-9]M)?([0-5]?[0-9]S)"
    request {
        method POST()
        url '/api/quizzes'
        headers {
            header(authorization(), $(p('Bearer toto'), c('Bearer HOST_USER')))
            header(contentType(), applicationJson())
        }
        def anyAnswer = [label: anyNonBlankString(), valid: anyBoolean().asBoolean()]
        def anyQuestion = [label: anyNonBlankString(), duration: regex(anyNewDuration), answers: [anyAnswer, anyAnswer, anyAnswer, anyAnswer]]
        body(
                title: anyNonBlankString(),
                questions: [anyQuestion, anyQuestion, anyQuestion]
        )
    }
    response {
        status 201
        body(
                id: $(regex(anyId)),
                duration: regex(anyDuration),
                title: fromRequest().body('$.title'),
                questions: $([fromRequest().body('$..questions[*]')])
        )
        bodyMatchers {
            jsonPath('$.questions', byType { minOccurrence(3) })
        }
        headers {
            header(expires(), '0')
            header(pragma(), 'no-cache')
            header('X-Frame-Options', 'DENY')
            header('Referrer-Policy', 'no-referrer')
            header('X-Content-Type-Options', 'nosniff')
            header('X-XSS-Protection', '1 ; mode=block')
            header(cacheControl(), 'no-cache, no-store, max-age=0, must-revalidate')
        }
    }
}
