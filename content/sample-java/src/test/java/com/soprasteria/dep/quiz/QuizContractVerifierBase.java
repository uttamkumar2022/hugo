package com.soprasteria.dep.quiz;

import com.soprasteria.dep.commons.test.DepContractVerifierBase;
import com.soprasteria.dep.commons.test.InMemoryDepContextManager;
import com.soprasteria.dep.quiz.model.DepTechnicalAuthBusinessContext;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base class for Spring cloud contract verifier tests that runs on producer side and that includes the quiz business context.
 */
@SpringBootTest
@AutoConfigureWebTestClient
public class QuizContractVerifierBase extends DepContractVerifierBase {

    @Autowired
    private InMemoryDepContextManager depContextDetailsService;

    @BeforeEach
    public void beforeEach() {
        super.beforeEach();
        depContextDetailsService.createDepContext(TestConstants.DEP_TECHNICAL_AUTH_BUSINESS_CONTEXT);
    }
}
