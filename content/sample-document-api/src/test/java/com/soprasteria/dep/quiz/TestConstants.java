package com.soprasteria.dep.quiz;

import com.soprasteria.dep.quiz.model.DepTechnicalAuthBusinessContext;
import org.apache.commons.lang3.StringUtils;

public final class TestConstants {

    public static final DepTechnicalAuthBusinessContext DEP_TECHNICAL_AUTH_BUSINESS_CONTEXT = new DepTechnicalAuthBusinessContext(
            "", "toto", "clientId", null, null, "", "", null, StringUtils.EMPTY);

}
