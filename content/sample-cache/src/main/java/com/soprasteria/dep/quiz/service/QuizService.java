package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.commons.error.DepBusinessCode;
import com.soprasteria.dep.commons.error.DepException;
import com.soprasteria.dep.commons.error.DepFeedback;
import com.soprasteria.dep.commons.mongodb.error.MongoErrorMappingsKt;
import com.soprasteria.dep.commons.page.Range;
import com.soprasteria.dep.quiz.config.QuestionProperties;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import com.soprasteria.dep.quiz.repository.QuizRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static java.lang.String.format;

@Service
public class QuizService {

    private static final Logger logger = LoggerFactory.getLogger(QuizService.class);

    public final QuizRepository quizRepository;

    private final QuestionProperties questionProperties;

    public QuizService(QuizRepository quizRepository, QuestionProperties questionProperties) {
        this.quizRepository = quizRepository;
        this.questionProperties = questionProperties;
    }

    public Flux<Quiz> findAll(Range range) {
        return quizRepository.findAll(range)
                             .doOnSubscribe(s -> logger.debug("Searching quizzes within {}", range))
                             .doOnComplete(() -> logger.debug("Quizzes within {} retrieved", range));
    }

    public Mono<Quiz> create(Quiz quiz) {
        return Mono.fromSupplier(() -> checkQuiz(quiz))
                   .flatMap(quizRepository::insert)
                   .doOnSubscribe(s -> logger.info("Creating {}", quiz))
                   .doOnSuccess(q -> logger.info("Created {}", q))
                   .onErrorMap(MongoErrorMappingsKt::mapSbsMongoError);
    }

    Quiz checkQuiz(Quiz quiz) {
        if (quiz.getQuestions().size() > questionProperties.getMax()) {
            throw tooManyQuestionsError(quiz);
        }
        return quiz;
    }

    private DepException tooManyQuestionsError(Quiz quiz) {
        String msg = format("Quiz %s has more questions than maximum authorized %s", quiz.getTitle(), questionProperties.getMax());
        return new DepFeedback.Builder(msg, DepBusinessCode.INVALID_CONTENT, logger::debug)
                .withSource(format("%s.questions#size", Quiz.class.getSimpleName()))
                .build()
                .toException();
    }

    private Mono<Quiz> idNotFoundError(ObjectId id) {
        String message = format("Cannot find student with id %s", id);
        return Mono.error(new DepFeedback(message, DepBusinessCode.RESOURCE_NOT_FOUND, logger::debug).toException());
    }
}
