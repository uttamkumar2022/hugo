package com.soprasteria.dep.quiz.resource;

import com.soprasteria.dep.commons.security.TypeAllowed;
import com.soprasteria.dep.commons.security.model.DefaultDepAuthContext;
import com.soprasteria.dep.quiz.mapper.QuizMapper;
import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import com.soprasteria.dep.quiz.service.SecureQuizService;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static com.soprasteria.dep.commons.api.ApiConstants.ID_PATH;
import static com.soprasteria.dep.quiz.resource.ResourceConstants.QUIZZES;

// This resource class is separated from QuizResource because our documentation is step by step and uses live code snippets.
// You MUST NEVER separate related domain logic is separated classes like this.
// If you follow the quickstart INCLUDE this method in QuizResource class instead
@RestController
@RequestMapping(QUIZZES)
class SecureQuizResource {

    private final SecureQuizService quizService;
    public SecureQuizResource(SecureQuizService quizService) {
        this.quizService = quizService;
    }

    ////////////////
    // Copy this to your QuizResource
    @DeleteMapping(ID_PATH)
    @TypeAllowed(DefaultDepAuthContext.class)
    public Mono<QuizApiDto> deleteById(@PathVariable ObjectId id) {
        return quizService.deleteById(id).map(QuizMapper::toApi);
    }
    //
    ////////////////
}
