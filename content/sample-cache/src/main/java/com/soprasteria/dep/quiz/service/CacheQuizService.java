package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.commons.cache.CacheExtensionsKt;
import com.soprasteria.dep.commons.cache.SimpleDepCache;
import com.soprasteria.dep.commons.error.DepBusinessCode;
import com.soprasteria.dep.commons.error.DepFeedback;
import com.soprasteria.dep.commons.mongodb.error.MongoErrorMappingsKt;
import com.soprasteria.dep.quiz.config.QuestionProperties;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import com.soprasteria.dep.quiz.repository.QuizRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static com.soprasteria.dep.commons.mongodb.ObjectIdMappingsKt.toObjectId;
import static java.lang.String.format;

@Service
public class CacheQuizService extends QuizService {

    private static final Logger logger = LoggerFactory.getLogger(CacheQuizService.class);

    private SimpleDepCache<String, Quiz> cacheQuiz;

    public CacheQuizService(QuizRepository quizRepository, QuestionProperties questionProperties, CacheManager cacheManager) {
        super(quizRepository, questionProperties);
        // Only copy this to your QuizService
        this.cacheQuiz = new SimpleDepCache<>(CacheExtensionsKt.cacheOrError(cacheManager, "cacheQuizByid"),
                                              p -> String.valueOf(p.getId()));
    }

    ////////////////
    // Only copy this to your QuizService
    public Mono<Quiz> create(Quiz quiz) {
        return Mono.fromSupplier(() -> checkQuiz(quiz))
                   .flatMap(quizRepository::insert)
                   .doOnSubscribe(s -> logger.info("Creating {}", quiz))
                   .doOnSuccess(q -> logger.info("Created {}", q))
                   .flatMap(this::storeInCache)
                   .onErrorMap(MongoErrorMappingsKt::mapSbsMongoError);
    }

    public Mono<Quiz> findOne(ObjectId id) {
        return cacheQuiz.switchIfEmpty(id.toString(), Quiz.class, this::findOneInRepository)
                        .switchIfEmpty(idNotFoundError(id));
    }

    private Mono<Quiz> findOneInRepository(String id) {
        return quizRepository.findById(toObjectId(id)).doOnSubscribe(s -> logger.debug("Searching student {}", id))
                             .doOnSuccess(s -> logger.debug("Found student {}", s));
    }

    private Mono<Quiz> storeInCache(Quiz quiz) {
        return Mono.just(quiz).doOnSubscribe(t -> logger.debug("Add following quiz in cache {}", quiz))
                   .flatMap(cacheQuiz::store).doOnNext(t -> logger.debug("Quiz {} stored in cache", quiz));
    }

    private Mono<Quiz> idNotFoundError(ObjectId id) {
        String message = format("Cannot find student with id %s", id);
        return Mono.error(new DepFeedback(message, DepBusinessCode.RESOURCE_NOT_FOUND, logger::debug).toException());
    }
    //
    ////////////////
}
