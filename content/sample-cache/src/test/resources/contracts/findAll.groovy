import org.springframework.cloud.contract.spec.Contract

Contract.make {
    def anyDuration = "PT([0-5]?[0-9]M)?([0-5]?[0-9]S)?"
    request {
        method GET()
        url '/api/quizzes'
        headers {
            header(authorization(), $(p('Bearer mirana'), c('Bearer ALL_TYPE')))
            header(contentType(), applicationJson())
        }
    }
    response {
        def anyAnswer = [label: anyNonBlankString(), valid: anyBoolean().asBoolean()]
        def anyQuestion = [label: anyNonBlankString(), duration: regex(anyDuration), answers: [anyAnswer, anyAnswer, anyAnswer, anyAnswer]]
        def anyQuiz = [title: anyNonBlankString(), questions: [anyQuestion, anyQuestion, anyQuestion], duration: regex(anyDuration)]
        status 206
        body([anyQuiz, anyQuiz, anyQuiz, anyQuiz])
        headers {
            header(expires(), '0')
            header(pragma(), 'no-cache')
            header('X-Frame-Options', 'DENY')
            header('Referrer-Policy', 'no-referrer')
            header('X-Content-Type-Options', 'nosniff')
            header('X-XSS-Protection', '1 ; mode=block')
            header(cacheControl(), 'no-cache, no-store, max-age=0, must-revalidate')
        }
    }
}
