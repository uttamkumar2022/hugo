package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.quiz.model.entity.Quiz;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static com.soprasteria.dep.commons.test.SecurityTestExtensionsKt.testWithDepContext;
import static com.soprasteria.dep.quiz.TestData.javaCollectionQuestion;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class CacheQuizServiceTest {

    private static final Duration CACHE_DELAY = Duration.ofMillis(10);
    private Cache cacheIds;

    @Autowired
    private CacheQuizService quizService;
    @Autowired
    CacheManager cacheManager;

    @PostConstruct
    public void initCache() {
        this.cacheIds = cacheManager.getCache("cacheQuizByid");
    }

    @Test
    void test_createUpdatesCache() {
        Quiz quiz = new Quiz().setTitle("Using cache in Java").setQuestions(Collections.singletonList(javaCollectionQuestion()));
        AtomicReference<String> generatedId = new AtomicReference<>();
        // Create
        testWithDepContext(quizService.create(quiz)).expectSubscription().consumeNextWith(s -> {
            assertThat(s).isEqualTo(quiz);
            AssertionsForInterfaceTypes.assertThat(s.getId()).isNotNull();
            generatedId.set(s.getId().toString());
        }).verifyComplete();

        // Search in cache
        assertThat(generatedId.get()).isNotNull();
        testWithDepContext(Mono.delay(CACHE_DELAY).map(d -> idFromCache(generatedId.get()))).expectSubscription()
                                                                                            .consumeNextWith(s -> assertThat(s).isPresent().hasValue(quiz)).verifyComplete();
    }

    private Optional<Quiz> idFromCache(String id) {
        return Optional.ofNullable(cacheIds).map(c -> c.get(id, Quiz.class));
    }
}
