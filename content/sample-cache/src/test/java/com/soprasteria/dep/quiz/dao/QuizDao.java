package com.soprasteria.dep.quiz.dao;

import com.soprasteria.dep.commons.test.MongoDao;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.soprasteria.dep.quiz.TestData.javaQuiz;
import static java.util.Arrays.asList;

@Component
public class QuizDao extends MongoDao<Quiz> {

    public QuizDao(MongoTemplate mongoTemplate) {
        super(Quiz.class, mongoTemplate);
    }

    @Override
    public List<Quiz> defaultEntities() {
        return asList(
                javaQuiz(),
                javaQuiz().withId(new ObjectId(0, 1)).setTitle("Kotlin")
        );
    }
}
