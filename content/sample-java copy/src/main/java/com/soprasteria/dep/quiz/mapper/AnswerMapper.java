package com.soprasteria.dep.quiz.mapper;

import com.soprasteria.dep.quiz.model.api.AnswerApiDto;
import com.soprasteria.dep.quiz.model.entity.Answer;

public final class AnswerMapper {

    private AnswerMapper() {
    }

    public static AnswerApiDto toApi(Answer input) {
        return new AnswerApiDto().setLabel(input.getLabel()).setValid(input.isValid());
    }

    public static Answer toEntity(AnswerApiDto input) {
        return new Answer().setLabel(input.getLabel()).setValid(input.isValid());
    }
}
