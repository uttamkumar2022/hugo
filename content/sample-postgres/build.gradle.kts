import com.soprasteria.dep.*

depJavaApplication {
    apiVersion = 1
    useJib = true
    usePropertiesCompletion = false
    useKapt = false
    useDok = false
    useDokDsl = false
}

dependencies {
    implementation(dep("api"))
    implementation(dep("jpa"))
    implementation(dep("security"))
    runtimeOnly(dep("monitor"))
    testImplementation(dep("test"))
}

val apiElements = project.configurations.named("apiElements")
val runtimeElements = project.configurations.named("runtimeElements")
val jar: Task = project.tasks.getByName("jar")
val bootJar: Task = project.tasks.getByName("bootJar")
setOf(apiElements, runtimeElements).forEach {
    it.get().outgoing.artifacts.removeIf { a -> a.buildDependencies.getDependencies(null).contains(jar) }
    it.get().outgoing.artifact(bootJar)
}
