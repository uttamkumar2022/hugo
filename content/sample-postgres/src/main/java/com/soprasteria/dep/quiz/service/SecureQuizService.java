package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.commons.error.DepException;
import com.soprasteria.dep.commons.error.DepFeedback;
import com.soprasteria.dep.commons.security.model.DepAuthContext;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import com.soprasteria.dep.quiz.repository.QuizRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprasteria.dep.commons.security.ReactiveSecurityContextKt.currentDepAuthContext;
import static java.lang.String.format;

// This service class is separated from QuizService because our documentation is step by step and uses live code snippets.
// You MUST NEVER separate related domain logic is separated classes like this.
// If you follow the quickstart INCLUDE this method in QuizService class instead
@Service
public class SecureQuizService {

    private static final Logger logger = LoggerFactory.getLogger(SecureQuizService.class);

    private final QuizRepository quizRepository;

    public SecureQuizService(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    ////////////////
    // Only copy this to your QuizService
    public Mono<Quiz> deleteById(String id) {
        return Mono.just(quizRepository.findById(id))
                   .filter(Optional::isPresent)
                   .switchIfEmpty(Mono.error(notFoundError(id)))
                   .flatMap(quiz -> delete(quiz.get()))
                   .doOnSubscribe(s -> logger.info("Deleting quiz {}", id));

    }

    private Mono<Quiz> delete(Quiz quiz) {
        return Mono.just(quiz)
                   .doOnSuccess(quiz1 -> quizRepository.deleteById(quiz1.getId()))
                   .then(logOnDeleted(quiz));

    }

    private Mono<Quiz> logOnDeleted(Quiz quiz) {
        return currentDepAuthContext()
                .map(DepAuthContext::getClientId)
                .defaultIfEmpty("unknown")
                .doOnNext(u -> logger.info("Quiz {} deleted by {}", quiz.getId(), u))
                .thenReturn(quiz);

    }

    private DepException notFoundError(String id) {
        return new DepFeedback(format("Quiz %s not found", id), RESOURCE_NOT_FOUND, logger::info).toException();
    }
    //
    ////////////////
}
