package com.soprasteria.dep.quiz.repository;

import com.soprasteria.dep.commons.jpa.GenericRepository;
import com.soprasteria.dep.quiz.model.entity.Quiz;

public interface QuizRepository extends GenericRepository<Quiz> {

}
