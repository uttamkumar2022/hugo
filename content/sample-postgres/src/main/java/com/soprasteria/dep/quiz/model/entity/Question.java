package com.soprasteria.dep.quiz.model.entity;

import com.soprasteria.dep.commons.jpa.JpaAuditable;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "question")
public class Question extends JpaAuditable {

    @NotNull
    @Column
    private String label;

    @NotNull
    @Column
    private long durationInSeconds;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Answer> answers = new ArrayList<>();

    public String getLabel() {
        return label;
    }

    public Question setLabel(String label) {
        this.label = label;
        return this;
    }

    public long getDurationInSeconds() {
        return durationInSeconds;
    }

    public Question setDurationInSeconds(long durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
        return this;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public Question setAnswers(List<Answer> answers) {
        this.answers = answers;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(label);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        Question question = (Question) o;
        return Objects.equals(label, question.label);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("label", label)
                .toString();
    }
}
