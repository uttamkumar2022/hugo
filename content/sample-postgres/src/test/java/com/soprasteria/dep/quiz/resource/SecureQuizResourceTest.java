package com.soprasteria.dep.quiz.resource;

import com.soprasteria.dep.quiz.dao.QuizDao;
import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.transaction.annotation.Transactional;

import static com.soprasteria.dep.commons.context.model.ContextConstants.BEARER_PREFIX;
import static com.soprasteria.dep.commons.test.TestConstants.DEFAULT_SUB;
import static com.soprasteria.dep.quiz.mapper.QuizMapper.toApi;
import static com.soprasteria.dep.quiz.resource.ResourceConstants.QUIZZES;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// This test is separated from QuizResourceTest because our documentation is step by step and uses live code snippets.
// You MUST NEVER separate related domain logic is separated classes like this.
// If you follow the quickstart INCLUDE this method in QuizResourceTest class instead
@SpringBootTest
@AutoConfigureWebTestClient
@AutoConfigureRestDocs
@Transactional
class SecureQuizResourceTest {

    private static final String DEFAULT_AUTHORIZATION_HEADER = BEARER_PREFIX + " " + DEFAULT_SUB;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private QuizDao quizDao;

    @BeforeEach
    void setup() {
        quizDao.initTest();
    }

    @Test
    void deleteById_shouldSucceedForAdministrator() {
        QuizApiDto quiz = toApi(quizDao.findAny());
        webTestClient.delete()
                     .uri(QUIZZES + "/{id}", quiz.getId())
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(QuizApiDto.class)
                     .isEqualTo(quiz);
    }

    @Test
    void deleteById_shouldFail_whenUnauthorized() {
        String id = toApi(quizDao.findAny()).getId();
        webTestClient.delete()
                     .uri(QUIZZES + "/{id}", id)
                     .exchange()
                     .expectStatus().isUnauthorized();
    }
}
