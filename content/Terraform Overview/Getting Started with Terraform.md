+++
title = "Getting Started with Terraform"
weight ="9"
+++



>Terraform can manage infrastructure on multiple cloud platforms.
The human-readable configuration language helps you write infrastructure code quickly.
Terraform's state allows you to track resource changes throughout your deployments.
You can commit your configurations to version control to safely collaborate on infrastructure.


**Using Terraform** has several advantages over manually managing your infrastructure:

- Terraform can manage infrastructure on multiple cloud platforms.
- The human-readable configuration language helps you write infrastructure code quickly.
- Terraform's state allows you to track resource changes throughout your deployments.
- You can commit your configurations to version control to safely collaborate on infrastructure.



![Terraform Workflow](../images/terraform-workflow.png)




#### To deploy infrastructure with Terraform:
- **Scope** - Identify the infrastructure for your project.
- **Author** - Write the configuration for your infrastructure.
- **Initialize** - Install the plugins Terraform needs to manage the infrastructure.
- **Plan** - Preview the changes Terraform will make to match your configuration.
- **Apply** - Make the planned changes. 

#### Track your infrastructure

Terraform keeps track of your real infrastructure in a state file, which acts as a source of truth for your environment. Terraform uses the state file to determine the changes to make to your infrastructure so that it will match your configuration.




### Install Terraform

To use Terraform you will need to install it. HashiCorp distributes Terraform as a binary package. You can also install Terraform using popular [package managers](https://www.terraform.io/downloads.html).

Once the Installation is completed you can enable auto completion using below command
```
terraform -install-autocomplete
```


#### Terraform with Azure 


Terraform on Azure

 [Follow this Link](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs)

- [ ] Install Az command to manage azure using cli
```
curl -sL https://azurecliprod.blob.core.windows.net/rhel7_6_install.sh | sudo bash
```
- [ ] Firstly, login to the Azure CLI using:
```
az login
```
- [ ] List all the subscription
```
az account list
```
- [ ] Set Default subscription
```
az account set --subscription="SUBSCRIPTION_ID"
```
- [ ]create the Service Principal which will have permissions to manage resources in the specified Subscription using the following command:

```
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/SUBSCRIPTION_ID"
```
- [ ] Test the current user login
```
az login --service-principal -u CLIENT_ID -p CLIENT_SECRET --tenant TENANT_ID
```
- [ ] check the vms in Azure
```
az vm list-sizes --location westus
```
- [ ] Logout once done 
```
az logout
```

- [ ] Craete a connection b/w Terraform and Azure Cloud
- [ ] Create one provider file as follows
```
vi provider.tf
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}
# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
  subscription_id = "This is subscription id"
  client_id       = "d1d1f260-f99c-4fc5-a7f9-23b7b828b1f9"
  client_secret   = "Pasword "
  tenant_id       = "This is Directory ID"
```
- [ ] Initialize the terraform to download drivers
```
terraform init
```
- [ ] Create a new tf file to create resource group
```
Page up
resource "azurerm_resource_group" "example" {
  name     = "example"
  location = "West Europe"
}
```





{{% notice info %}}
Terraform understands only files with .tf format
{{% /notice %}}
